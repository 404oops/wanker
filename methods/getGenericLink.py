import re

def getGenericLink(inputstring):
    services = [
        ("https://www.deezer.com", "(track|album)/[^ \t\n\r\f\v]+"),
        ("https://open.spotify.com", "(track|album)/(.{22})"),
        ("https://music.apple.com", "\\w{2}/(album|song)/[^?\\s]+\\S*"),
        ("https://music.youtube.com", "watch\\?v=(.{11})"),
        ("https://music.yandex.ru*", "(track|album)/(.{8})")
    ]
    collection = re.split('[\s\n]+', inputstring)
    toreturn = []
    for i in collection:
        for service, pattern in services:
            if match := re.search(f"{service}/{pattern}", i):
                toreturn.append(match.group())
    
    return toreturn or None

if __name__ == "__main__":
    print(getGenericLink("""https://open.spotify.com/track/2ZaTpgdhU8MKpiujnBGPoa?si=f0c6f6719a3f4342
https://open.spotify.com/track/7nsO4eXkY7DvrWbjYUDLJS?si=25128f83e71b468b
https://open.spotify.com/track/3gVhsZtseYtY1fMuyYq06F?si=cd23d8a48cc540e5"""))