from methods.downloadPreview import downloadPreview
from methods.flop import flop
from methods.getData import getData

import contextlib
import logging

async def getEverything(message, errormsg, sp, messagecontent, reply):
    logging.info("Passed URL Checks, grabbing from song.link API")
    dB = getData(str(messagecontent))
    pageurl = dB["pageUrl"]
    if "song" in pageurl:
        linktype = "song"
    elif "album" in pageurl:
        linktype = "album"
    logging.info("API Contacted.")
    try:
        dB["linksByPlatform"]
    except Exception as exception:
        await flop(message, dB, exception)
    
    logging.info("Successfully initialized the database")
    lbp = dB["linksByPlatform"]
    streaming = {"spotify", "youtubeMusic", "deezer", "yandex"} # Omit appleMusic since it's non-standard

    allsongs = {item:
        lbp[item]["url"]
        for item in streaming
        if item in lbp
    }  # get each streaming service and their corresponding links
    allsongs["odesli"] = pageurl
    with contextlib.suppress(Exception):
        allsongs["appleMusic"] = lbp["appleMusic"]["url"].split('&')[0] # get the separate link for apple music
    logging.info("Grabbed all URIs, Getting metadata...")
    try:
        if 'spotify' not in allsongs:
            logging.info("Failed to get ANY spotify link, omitting..")
            file_path = ""
            try:
                lbp['appleMusic']
            except Exception:
                await message.reply(
                    errormsg,
                    mention_author=True
                )
            appleMusic_meta = dB['entitiesByUniqueId'][lbp['appleMusic']['entityUniqueId']]
            artists = appleMusic_meta['artistName']
            name = appleMusic_meta['title']
        elif linktype == "song":
            logging.info("Song is available on spotify, trying to grab preview with the metadata...")
            meta = sp.tracks([allsongs["spotify"]])["tracks"][0]
            artists = dB['entitiesByUniqueId'][lbp['spotify']['entityUniqueId']]['artistName']
            name = meta['name']
            preview_url = sp.track(meta['uri'])['preview_url']
            file_path = downloadPreview(meta, preview_url)
        else:
            meta = sp.albums([allsongs["spotify"]])['albums'][0]
            print(meta)
            artists = dB['entitiesByUniqueId'][lbp['spotify']['entityUniqueId']]['artistName']
            name = meta['name']
            file_path = ""
        
    except Exception as exception:
        await flop(message, dB, exception, reply)
    return allsongs,file_path,artists,name,linktype