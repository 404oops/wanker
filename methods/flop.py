from config import flop_path
import json
import os
from methods import mkRandom

async def flop(message, dB, exception, reply):
    errorcode = mkRandom()
    finaldir = os.path.join(flop_path, errorcode)
    os.mkdir(finaldir)
    open(os.path.join(finaldir, "info.txt"), 'x').write(
        f"""Message Author: {str(message.author.id)}
Message Content: {str(message.content)}
Exception: {exception}""")
    open(os.path.join(finaldir, "response.json"), 'x').write(json.dumps(dB))
    await reply.edit(f"Oops! Looks like I failed getting your songs. Contact the owner with the flopcode:`{errorcode}`", mention_author=True)