import sqlite3

def create_database(db_name):
    conn = sqlite3.connect(db_name)
    c = conn.cursor()

    # Create table to store ALLOWLIST
    c.execute('''CREATE TABLE IF NOT EXISTS ALLOWLIST
                (id INTEGER PRIMARY KEY)''')

    # Create table to store MODLIST
    c.execute('''CREATE TABLE IF NOT EXISTS MODLIST
                (id INTEGER PRIMARY KEY)''')

    # Create table to store DENYLIST (optional)
    c.execute('''CREATE TABLE IF NOT EXISTS DENYLIST
                (id INTEGER PRIMARY KEY)''')
    
        # Create table to store counter
    c.execute('''CREATE TABLE IF NOT EXISTS COUNTER
                (count INTEGER DEFAULT 0)''')

    conn.commit()
    conn.close()

def add_to_denylist(db_name, id):
    conn = sqlite3.connect(db_name)
    c = conn.cursor()

    # Insert the ID into the DENYLIST table
    c.execute("INSERT INTO DENYLIST (id) VALUES (?)", (id,))

    conn.commit()
    conn.close()

def add_to_allowlist(db_name, id):
    conn = sqlite3.connect(db_name)
    c = conn.cursor()

    # Insert the ID into the ALLOWLIST table
    c.execute("INSERT INTO ALLOWLIST (id) VALUES (?)", (id,))

    conn.commit()
    conn.close()

def get_allowlist(db_name):
    conn = sqlite3.connect(db_name)
    c = conn.cursor()

    # Retrieve all IDs from the ALLOWLIST table
    c.execute("SELECT id FROM ALLOWLIST")
    allowlist = [row[0] for row in c.fetchall()]

    conn.close()

    return allowlist

def get_denylist(db_name):
    conn = sqlite3.connect(db_name)
    c = conn.cursor()

    # Retrieve all IDs from the DENYLIST table
    c.execute("SELECT id FROM DENYLIST")
    denylist = [row[0] for row in c.fetchall()]

    conn.close()

    return denylist

def get_modlist(db_name):
    conn = sqlite3.connect(db_name)
    c = conn.cursor()

    # Retrieve all IDs from the DENYLIST table
    c.execute("SELECT id FROM MODLIST")
    modlist = [row[0] for row in c.fetchall()]

    conn.close()

    return modlist

def add_to_modlist(db_name, id):
    conn = sqlite3.connect(db_name)
    c = conn.cursor()

    # Insert the ID into the MODLIST table
    c.execute("INSERT INTO MODLIST (id) VALUES (?)", (id,))

    conn.commit()
    conn.close()

def remove_from_allowlist(db_name, id):
    conn = sqlite3.connect(db_name)
    c = conn.cursor()

    # Delete the ID from the ALLOWLIST table
    c.execute("DELETE FROM ALLOWLIST WHERE id=?", (id,))

    conn.commit()
    conn.close()

def remove_from_denylist(db_name, id):
    conn = sqlite3.connect(db_name)
    c = conn.cursor()

    # Delete the ID from the DENYLIST table
    c.execute("DELETE FROM DENYLIST WHERE id=?", (id,))

    conn.commit()
    conn.close()

def increment_counter(db_name):
    conn = sqlite3.connect(db_name)
    c = conn.cursor()

    # Check if the COUNTER table is empty®
    c.execute("SELECT count(*) FROM COUNTER")
    row = c.fetchone()
    is_empty = row[0] == 0 if row else True

    if is_empty:
        # Insert the initial counter value of 1
        c.execute("INSERT INTO COUNTER (count) VALUES (1)")
        current_count = 1
    else:
        # Read the current counter value
        c.execute("SELECT count FROM COUNTER")
        current_count = c.fetchone()[0] + 1

        # Update the counter value in the table
        c.execute("UPDATE COUNTER SET count=?", (current_count,))

    conn.commit()
    conn.close()

    return current_count


def get_counter(db_name):
    conn = sqlite3.connect(db_name)
    c = conn.cursor()

    # Read the current counter value
    c.execute("SELECT count FROM COUNTER")
    row = c.fetchone()
    current_count = row[0] if row else 0
    conn.close()

    return current_count