from methods.getGenericLink import getGenericLink
import requests
import json


def getData(link):
    return json.loads(
        requests.get(
            "https://api.song.link/v1-alpha.1/links",
            params={
                "url": getGenericLink(link),
                "userCountry": "RS",
            },
        ).text
    )