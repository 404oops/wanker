import nextcord
from methods import databasing
from config import db
async def updateStatus(client):
    await client.change_presence(
        activity=nextcord.Activity(
            type=nextcord.ActivityType.listening,
            name=f"links in {len(client.guilds)} servers with {databasing.increment_counter(db)} messages processed."
        )
    )