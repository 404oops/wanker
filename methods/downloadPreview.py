from config import cache_directory

import logging
import os
import subprocess

def downloadPreview(meta, preview_url):
    if preview_url is not None:
        try:
            file_name = f'{str(meta["external_ids"]["isrc"])}.mp3'
            logging.info(f"Downloading - link: {preview_url}, file: {file_name}")
            if not os.path.exists(cache_directory):
                os.mkdir(cache_directory)
            file_path = os.path.join(cache_directory, file_name)

            if not os.path.exists(file_path):
                subprocess.run(["wget", preview_url, "-4", "-O", file_path], stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL)

            logging.info("Downloaded preview.")
        except Exception:
            file_path = ""
    else: file_path = ""
    return file_path