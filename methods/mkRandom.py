import random


def mkRandom(): return "".join(random.choice(list("abcdefghijklmnopqrstuvwxyz0123456789")) for _ in range(5))