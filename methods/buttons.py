import nextcord

class buttons(nextcord.ui.View):
    def __init__(self, allsongs):
        super().__init__()
        service_emojis = {
            "spotify": ":spotify:1110875969838923787",
            "youtubeMusic": ":ytmusic:1110875961001525258",
            "deezer": ":deezer:1110875962725380146",
            "appleMusic": ":applemusic:1110875965606871082",
            "yandex": ":yandexmusic:1124340290988290141",
            "odesli": ":odesli:1152947117799780413"
        }
        for service, link in allsongs.items():
            serviceemoji = service_emojis[service]
            self.add_item(
                nextcord.ui.Button(
                    emoji=nextcord.PartialEmoji.from_str(serviceemoji),
                    url=link
                )
            )