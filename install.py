import os
os.system("apt install ffmpeg neofetch python-pip -y")
os.system("python3 -m pip install -r req.txt --break-system-packages")
from config import db, botowner
from methods import databasing
for path in os.listdir():
    if os.path.isdir(path):
        os.system(f"rm -rf {path}/*cache*")
    else:
        os.system("rm -rf *cache*")
os.system(f"rm .cache {db}")


databasing.create_database(db)
databasing.add_to_modlist(db, botowner)
databasing.add_to_allowlist(db, botowner)
