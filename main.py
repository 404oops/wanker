import contextlib, json
import logging, subprocess, os, re
import spotipy, nextcord
from config import token, spotifyid, spotifysecret, db, ytcache, botowner
from methods import getEverything, getGenericLink, buttons, getData, updateStatus, databasing, mkRandom

logging.basicConfig(level=logging.INFO)
errormsg = """# Oops! Looks like your song link is **broken**. **You're not at fault here.**
## What can I do?
For now: Nothing. You can either contact 404oops directly (@404oops on Discord) with the link you used. I'll take it from there."""

intents = nextcord.Intents.default()
intents.message_content = True
intents.members = True
client = nextcord.Client(intents=intents)

# == Spotify API credentials ==
client_credentials_manager = spotipy.oauth2.SpotifyClientCredentials(
    client_id=spotifyid,
    client_secret=spotifysecret
)
sp = spotipy.Spotify(client_credentials_manager=client_credentials_manager)

# === DATABASE ===
databasing.create_database(db)
allowlist = databasing.get_allowlist(db)

@client.event
async def on_ready():
    logging.info("Bot is ready to rumble!")
    await updateStatus(client)

@client.event
async def on_message(message):
    if getGenericLink(message.content) != None:
        await updateStatus(client)
        async with message.channel.typing():
            for i in getGenericLink(message.content):
                reply = await message.reply(
                    "Processing...",
                    mention_author=True
                )
                
                allsongs, file_path, artists, songname, mediatype = await getEverything(message, errormsg, sp, i, reply)

                if file_path != "":
                    await reply.edit(
                        f"## {songname} by {artists}" 
                            if file_path != "" 
                        else 
                            f"## {songname} by {artists}\n### Preview is not available.",
                        file=nextcord.File(file_path),
                        view=buttons(allsongs)
                    )
                else:
                    await reply.edit(
                        f"## {songname} by {artists}" 
                            if file_path != "" 
                        else 
                            f"## {songname} by {artists}\n### Preview is not available."
                        if mediatype == "song"
                        else
                            f"## {songname} by {artists}",
                        view=buttons(allsongs)
                    )
        
        logging.info("All Done!")

    if client.user in message.mentions and (not message.reference or message.reference.resolved.author == client.user):
        desc = """musique+ is a music toolkit!
        I'll help you convert links and show song metadata! (More features coming soon!)
        To use me, just send a link to a song in any channel I can access, and then I'll reply to you with other links!"""
        if message.guild.get_member(991414141497647124):
            desc += "\n\nдолбоёб has been detected in this server. Please be advised that we **do the same functions**, except I give you links to other streaming services. I do **not** advise you to kick долбоёб off this server, since more features are coming in later versions."
        embed = nextcord.Embed(
            title="Hi!",
            description=desc,
            color=0xed3b3b
        )
        await message.reply(embed=embed)

@client.slash_command(description="Gives you metadata about your song or album")
async def metadata(
        interaction: nextcord.Interaction,
        link: str = nextcord.SlashOption(description="Song/Album link")
    ):
    if getGenericLink(link) is None:
        await interaction.response.send_message("You must specify a valid link to a song or album.", ephemeral=True)
    else:
        logging.info("Got valid link, proceeding")
        dB = getData(link)
        pageurl = dB["pageUrl"]
        if "song" in pageurl:
            mediatype = "song"
        elif "album" in pageurl:
            mediatype = "album"
        try:
            spotifylink = dB["linksByPlatform"]["spotify"]["url"].split("/")[-1].split("?")[0]
        except Exception:
            await interaction.response.send_message("Your song isn't on Spotify, so we can't get your metadata. Sorry!")
        if mediatype == "album":
            albumdata = sp.albums([spotifylink])['albums'][0]
            m, s = divmod(
            round(
                sum(
                    i['duration_ms']
                    for i in albumdata['tracks']['items']
                )
                /1000
                ), 60
            )
            if s < 10:
                s = f"0{s}"
            artists = []
            for allartists in albumdata['tracks']['items']:
                for artist in allartists['artists']:
                    if artist['name'] not in artists: artists.append(artist['name'])
            concatenated_names = concatNames(artists)
            toreturn = nextcord.Embed(title=f"{albumdata['name']} by {concatenated_names}", color=0xed3b3b)
            toreturn.set_thumbnail(url=albumdata['images'][0]['url'])
            data = {
                "Release Date:": albumdata['release_date'],
                "Copyright:": albumdata['copyrights'][0]['text'],
                "Publishing label:": albumdata['label'],
                "UPC Code:": albumdata['external_ids']['upc'],
                "Song count:": str(albumdata['tracks']['total']),
                "Total duration:": f"{m}:{s}"
            }
        elif mediatype == "song":
            logging.info("Detected song; Continuing...")
            meta = sp.tracks([spotifylink])["tracks"][0]
            albumdata = sp.albums([meta['album']['id']])
            names = [artist["name"] for artist in meta["artists"]]
            # get multiple artist names from metadata
            concatenated_names = concatNames(names)
            toreturn = nextcord.Embed(title=f"{meta['name']} by {concatenated_names}", color=0xed3b3b)
            toreturn.set_thumbnail(url=meta['album']['images'][0]['url'])
            data = {
                "Place in album:": f"{meta['track_number']}/{meta['album']['total_tracks']}",
                "Album:": meta['album']['name'],
                "Release Date:": meta['album']['release_date'],
                "ISRC:": meta['external_ids']['isrc'],
                "Copyright:": albumdata["albums"][0]['copyrights'][0]["text"],
            }
        lbp = dB["linksByPlatform"]
        streaming = {"spotify", "youtubeMusic", "deezer", "yandex"} # Omit appleMusic since it's non-standard

        allsongs = {item:
            lbp[item]["url"]
            for item in streaming
            if item in lbp
        }  # get each streaming service and their corresponding links
        allsongs["odesli"] = pageurl
        with contextlib.suppress(Exception):
            allsongs["appleMusic"] = lbp["appleMusic"]["url"].split('&')[0] # get the separate link for apple music
        logging.info("Buttons retreived")
        for key, value in data.items(): 
            toreturn.add_field(name=str(key), value=value, inline=True)
        toreturn.set_footer(text="Info delivered by Spotify API. Link delivered by odesli.co.")
        logging.info("Sending response. Good luck and godspeed")
        await interaction.response.send_message(
            embed=toreturn,
            view=buttons(allsongs)
        )
        logging.info("Attempted to send. Did you get it?")

def concatNames(names):
    if not 1 < len(names) < 3 and len(names) <= 1:
        return names[0]
    names[-1] = f"and {names[-1]}"
    return ", ".join(names)

@client.slash_command(description="Debug data")
async def debug(interaction: nextcord.Interaction):
    if interaction.user.id == botowner:
        server_list = '\n'.join([f"{server.name} - {server.id}" for server in client.guilds])
        output = subprocess.check_output(["neofetch", "--stdout"]).decode("utf-8")
        output += f"Server List:\n{server_list}\n"
        output += f"\nNumber of Servers: {len(client.guilds)}"
        output += f"\nnextcord Version: {nextcord.__version__}"
        embed = nextcord.Embed(title="System Information", description=f"```{output}```", color=0xed3b3b)
        logging.info("Debug command received and sent to owner.")
        await interaction.response.send_message(embed=embed, ephemeral=False)
    else:
        logging.error("Debug command received, not sent because executor isn't the owner.")
        await interaction.response.send_message("You don't have permissions to view the debug info.", ephemeral=True)

@client.slash_command(description="Download anything as mp3 using yt-dlp. Only available for allowed users to prevent abuse.")
async def ytdlp(
    interaction: nextcord.Interaction,
    link: str = nextcord.SlashOption(description="Video link"),
    publicize: bool = nextcord.SlashOption(description="Publicize the MP3 output.")
    ):
    global allowlist
    if interaction.user.id in allowlist or interaction.user.id in databasing.get_modlist(db):
        print(link)
        await interaction.response.defer(with_message=True, ephemeral=(not publicize))
        if not os.path.exists(ytcache):
            os.mkdir(ytcache)
        os.chdir(ytcache)
        uuid = mkRandom() # Make uuid for multiple consecutive command calls
        os.system(f'yt-dlp -x --audio-format mp3 \"{link}\" -o \"{uuid}/%(title)s.%(ext)s\"')
        filename = os.listdir(uuid)[0]
        await interaction.send(
            file=nextcord.File(f"{uuid}/{filename}"),
            ephemeral=(not publicize)
        )
        os.system(f'rm -rf {re.escape(uuid)}')
        os.chdir("..")
    else:
        await interaction.response.send_message("You don't have permissions.", ephemeral=True)


@client.slash_command(description="Add user to allowlist.")
async def allow(
    interaction: nextcord.Interaction,
    userid: str = nextcord.SlashOption(description="User ID"),
    announce: bool = nextcord.SlashOption(description="Announce")
    ):
    if interaction.user.id in databasing.get_modlist(db):
        userid = int(userid)
        with contextlib.suppress(Exception):
            databasing.add_to_allowlist(db, userid)
        global allowlist
        allowlist = databasing.get_allowlist(db)
        await interaction.response.send_message(
            f"Added user \"{client.get_user(userid).name}\" with display name \"{client.get_user(userid).display_name}\" and ID {userid}",
            ephemeral=announce
            )
        embed = nextcord.Embed(title="Before you begin...", description="""```Hello!
You may have noticed that you are now allowed to use the /ytdlp slash command. You HAVE to agree to these terms before you can use this command.
1. You must only provide links. Note that this command does not filter links you provide, so nothing but the links are not allowed.
2. The command that gets sent is: 'yt-dlp -x --audio-format mp3 \"link\" -o \"uuid/%(title)s.%(ext)s\"'. Please respect the syntax of this command.
3. Avoid trolling. No one wants the command line alternative to Bobby Tables.
4. You must agree to these terms before you can use this command.
5. You mustn't abuse this command with copyrighted content.
Copyrighted content is not allowed to be distributed without proper authorization by the copyright owner.

If you do not agree to these terms, let us know. We will disable this command for you.```""", color=nextcord.Color.red())
        await client.get_user(userid).send(embed=embed)
    else:
        await interaction.response.send_message("Cannot add user to allowlist. You're not a moderator ;)", ephemeral=True)
client.run(token)